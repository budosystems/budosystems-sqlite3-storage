#  Copyright (c) 2021.  Budo Systems
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

import pytest

from budosystems.models.core import Entity
from budosystems.xtra.sqlite3_storage.schemata import Schemata

class TestEntitySchema:
    def test_generate_schema_class(self):
        schema = Schemata.get(Entity)
        assert schema.source_class == Entity

    def test_table_name_property(self):
        schema = Schemata.get(Entity)
        assert schema.table_name == 'entity'

    def test_column_names_property(self):
        schema = Schemata.get(Entity)
        assert schema.column_names == ['id_name', 'entity_id']

    def test_select_by_id_query(self):
        schema = Schemata.get(Entity)
        assert schema.select_by_id_query == \
               'SELECT id_name, entity_id FROM entity AS t WHERE t.entity_id = :entity_id;'

    def test_insert_query(self):
        schema = Schemata.get(Entity)
        assert schema.insert_query == \
               'INSERT INTO entity AS t (id_name, entity_id) VALUES (:id_name, :entity_id);'

    def test_update_query(self):
        schema = Schemata.get(Entity)
        assert schema.update_query == \
               'UPDATE entity AS t SET (id_name, entity_id) = (:id_name, :entity_id) ' \
               'WHERE t.entity_id = :entity_id;'

    def test_upsert_query(self):
        schema = Schemata.get(Entity)
        assert schema.upsert_query == \
               'INSERT INTO entity AS t (id_name, entity_id) VALUES (:id_name, :entity_id) ' \
               'ON CONFLICT (entity_id) DO UPDATE ' \
               'SET (id_name, entity_id) = (:id_name, :entity_id) ' \
               'WHERE t.entity_id = :entity_id;'

    def test_delete_by_id_query(self):
        schema = Schemata.get(Entity)
        assert schema.delete_by_id_query == \
               'DELETE FROM entity AS t WHERE t.entity_id = :entity_id;'

    def test_table_definition(self):
        schema = Schemata.get(Entity)
        assert schema.table_definition == \
               'CREATE TABLE entity (id_name TEXT NOT NULL, entity_id TEXT PRIMARY KEY);'
