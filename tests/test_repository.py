#  Copyright (c) 2021.  Budo Systems
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

from pytest import fixture
import sqlite3
from typing import Any

from budosystems.xtra.pytest_suite.repository import AbstractTestRepository
from budosystems.storage.repository import Repository
from budosystems.xtra.sqlite3_storage.repository import SQLite3Repository


def test_import_from_core() -> None:
    assert isinstance(Repository, type)


def test_import_from_xtra() -> None:
    assert issubclass(SQLite3Repository, Repository)


class TestSQLite3Repository(AbstractTestRepository):

    # def test_sql_version(self):
    #     assert sqlite3.sqlite_version_info >= (3, 24, 0)

    @fixture(scope="class")
    def repo_class(self) -> type[Repository]:
        return SQLite3Repository

    @fixture(scope="class")
    def repo_args(self) -> dict[str, Any]:
        return {"con": sqlite3.connect(":memory:")}

    @fixture(scope="class")
    def repo_inaccessible(self, repo_class: type[Repository]) -> Repository:
        con = sqlite3.connect(":memory:")
        repo = repo_class(con=con)
        con.close()
        return repo
