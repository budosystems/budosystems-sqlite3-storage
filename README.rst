Budo Systems SQLite3 Storage
============================

Budo Systems storage implementation using SQLite3 which is part of the Python Standard Library.

Really just meant as a reference implementation for more robust DBMS.
