#  Copyright (c) 2021-2023.  Budo Systems
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

"""Schema factory and custom schemata for `Entity` tables."""

from __future__ import annotations

from collections.abc import Collection
from typing import Any, Optional, Union, get_origin, get_args
from attr import fields, resolve_types

from parallel_hierarchy import ParallelFactory

from budosystems.models.core import Entity

class MetaSchema(type):
    """
    Metaclass for the SQLite3 schema counterpart of Entity classes.
    """
    # def __new__(mcs, cls_name: str, bases: tuple[type, ...], namespace: dict[str, Any]):
    #     return super().__new__(mcs, cls_name, bases, namespace)

    source_class: Optional[type[Entity]]

    def __init__(cls, cls_name: str, bases: tuple[type, ...], namespace: dict[str, Any]):
        super().__init__(cls_name, bases, namespace)

        if not hasattr(cls, "source_class") or cls.source_class is None:
            cls.source_class = Entity
        cls._table_name = cls.source_class.__name__.casefold()

        # pylint: disable=not-an-iterable
        cls._column_names = [f.name for f in fields(cls.source_class)]
        cls._build_table_def()  # pylint: disable=no-value-for-parameter
        cls._build_queries()    # pylint: disable=no-value-for-parameter

    # @property
    # def source_class(cls) -> type[Entity]:
    #     return cls._source_class

    @property
    def table_name(cls) -> str:
        """Name of the SQL table corresponding to this schema."""
        return cls._table_name

    @property
    def column_names(cls) -> list[str]:
        """List of SQL column names corresponding to this schema's fields."""
        return cls._column_names

    @property
    def select_by_id_query(cls) -> str:
        """The SQL SELECT query."""
        return cls._select_by_id_query

    @property
    def insert_query(cls) -> str:
        """The SQL INSERT query."""
        return cls._insert_query

    @property
    def update_query(cls) -> str:
        """The SQL UPDATE query."""
        return cls._update_query

    @property
    def upsert_query(cls) -> str:
        """The SQL query to insert if an entity doesn't exist, or update if it already exists."""
        return cls._upsert_query

    @property
    def delete_by_id_query(cls) -> str:
        """The SQL DELETE query."""
        return cls._delete_by_id_query

    @property
    def table_definition(cls) -> str:
        """The SQL CREATE TABLE query."""
        return cls._table_definition

    def _build_table_def(cls) -> None:
        """
        Generates a string to properly create the table for the `Entity` class this
        `Schema` class represents.
        """
        table_def = f"CREATE TABLE {cls.table_name} "
        columns = []
        if not cls.source_class:
            return

        src_cls = resolve_types(cls.source_class)

        for f in fields(src_cls):  # pylint: disable=not-an-iterable
            type_origin = get_origin(f.type)
            type_args = get_args(f.type)
            nullable = "" if type_origin == Union and type(None) in type_args else " NOT NULL"

            f_type = MetaSchema._field_type(f.type, type_origin, type_args)
            col = MetaSchema._column(f, f_type, nullable)

            columns.append(col)

        table_def += "(" + ', '.join(columns) + ");"

        cls._table_definition = table_def

    @staticmethod
    def _field_type(f_type: type, type_origin: Optional[Any], type_args: tuple[Any, ...]) -> type:
        if type_origin is not None and len(type_args) > 0:
            if type_origin == Union:
                if len(type_args) == 1:
                    f_type = type_args[0]
                elif type(None) in type_args and len(type_args) == 2:
                    f_type = type_args[0] if type_args[0] is not type(None) else type_args[1]
                elif str in type_args:
                    f_type = str
                else:
                    f_type = object
            elif issubclass(type_origin, Collection):
                f_type = type_origin
        return f_type

    @staticmethod
    def _column(field: Any, f_type: type, null_constraint: str) -> str:
        col = str(field.name)

        if field.name == 'entity_id':
            col += " TEXT PRIMARY KEY"
        elif f_type is None:
            pass
        elif issubclass(f_type, Entity):
            # Define as foreign key
            foreign_schema = Schemata.get(field.type)
            col += f" TEXT REFERENCES {foreign_schema.table_name} (entity_id)" + null_constraint
        elif issubclass(f_type, str):
            col += " TEXT" + null_constraint
        elif issubclass(f_type, int):
            col += " INT" + null_constraint
        elif issubclass(f_type, float):
            col += " REAL" + null_constraint
        else:
            col += " BLOB" + null_constraint

        return col

    def _build_queries(cls) -> None:
        col_names = ", ".join(cls.column_names)

        # pylint: disable=not-an-iterable
        col_values = ", ".join([f":{f}" for f in cls.column_names])

        part = f"INSERT INTO {cls.table_name} AS t ({col_names}) VALUES ({col_values})"
        insert_query = part
        upsert_query = part

        upsert_query += " ON CONFLICT (entity_id) DO UPDATE "

        update_query = f"UPDATE {cls.table_name} AS t "

        part = f"SET ({col_names}) = ({col_values})"
        upsert_query += part
        update_query += part

        select_by_id_query = f"SELECT {col_names} FROM {cls.table_name} AS t"

        delete_by_id_query = f"DELETE FROM {cls.table_name} AS t"

        part = " WHERE t.entity_id = :entity_id"
        upsert_query += part
        update_query += part
        select_by_id_query += part
        delete_by_id_query += part

        # delete_by_id_query += " RETURNING *"  # not supported in SQLite < 3.35

        cls._insert_query = insert_query + ";"
        cls._upsert_query = upsert_query + ";"
        cls._update_query = update_query + ";"
        cls._select_by_id_query = select_by_id_query + ";"
        cls._delete_by_id_query = delete_by_id_query + ";"


class Schema(metaclass=MetaSchema):
    """
    Base class for the SQLite3 schema counterpart of Entity classes.
    """


Schemata = ParallelFactory(Entity, Schema, suffix="Schema")
Schemata.__doc__ = "Factory / repository of SQLite3 Entity Schema classes."
