#  Copyright (c) 2021.  Budo Systems
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

"""
Budo Systems storage implementation using SQLite3 which is part of the Python Standard Library.
"""

try:
    from ._version import version
except ImportError:
    from importlib.metadata import version as _version_fn, PackageNotFoundError

    try:
        version = _version_fn("budosystems-sqlite3-storage")
    except PackageNotFoundError:
        version = "version-undefined"
