#  Copyright (c) 2021-2023.  Budo Systems
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

"""Implementation of `budosystems.storage.repository` for SQLite3 storage."""

from __future__ import annotations

import sqlite3
import warnings
from typing import Any, Optional
from collections.abc import MutableMapping
from sqlite3 import Connection, Cursor, Row
from uuid import UUID
import pickle

from attr import fields

from budosystems.models.core import Entity
from budosystems.storage.query import Query
from budosystems.storage.repository import (
    Repository, SaveOption, ET, EntityNotFound,
    EntitySaveFailed, EntityReadFailed, EntityDeleteFailed,
    EntityAlreadyExists, RepositoryError, RepositoryNotAccessible
)

from .schemata import Schemata

def _dict_factory(cursor: Cursor, row: Row) -> MutableMapping[str, Any]:
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


class SQLite3Repository(Repository):
    """Implementation of the Repository interface for SQLite3 storage."""

    def __init__(self, con: Connection):
        self._con = con
        try:
            self._con.row_factory = _dict_factory
            tables = con.execute(
                "SELECT name FROM sqlite_schema WHERE type ='table' AND name NOT LIKE 'sqlite_%';"
            )

            self._tables = set(r[0] for r in tables.fetchall())
        except sqlite3.ProgrammingError as pe:
            raise RepositoryNotAccessible() from pe
        except sqlite3.DatabaseError:
            self._tables = set()

    def match(self, entity_type: type[ET], data: dict[str, Any]) -> list[ET]:
        schema = Schemata.get(entity_type)
        col_names = ", ".join(schema.column_names)
        where = []
        for k in data.keys():
            if k not in schema.column_names:
                raise KeyError()
            where.append(f"{k} = :{k}")
        where_clause = " WHERE " + " AND ".join(where)
        query = f"SELECT {col_names} FROM {schema.table_name}"
        if where:
            query += where_clause
        query += ";"
        entities: list[ET] = []
        try:
            results = self._con.execute(query, data)
            for row in results:
                entities.append(entity_type(**row))
        except sqlite3.OperationalError:
            pass
        except sqlite3.ProgrammingError as pe:
            raise EntityReadFailed(
                reason=RepositoryNotAccessible()
            ) from pe
        return entities

    def load(self, entity_type: type[ET], entity_id: UUID) -> ET:
        """
        Load and return an Entity with an ID matching the parameter.

        :param entity_type: The specific type of `Entity` expected.
        :param entity_id: The ID used to search the repository.
        :returns: The Entity matching the ID.
        :raises EntityNotFound: If there are no entities with the given ID in the repository.
        """
        schema = Schemata.get(entity_type)
        try:
            cur = self._con.cursor()
            result = cur.execute(
                    schema.select_by_id_query,
                    {"entity_id": str(entity_id)}
            ).fetchone()
            if not result:
                raise EntityReadFailed(
                    reason=EntityNotFound(
                            f"Could not find {entity_type.__name__} with id {entity_id}."
                    )
                )
            return entity_type(**result)
        except sqlite3.OperationalError as oe:
            raise EntityReadFailed(
                reason=TypeError(f"No table for type {entity_type.__name__}.")
            ) from oe
        except sqlite3.ProgrammingError as pe:
            raise EntityReadFailed(
                reason=RepositoryNotAccessible()
            ) from pe

    def save(self, entity: Entity, save_option: SaveOption = SaveOption.create_or_update) -> None:
        """
        Store the provided entity.

        :param entity: The entity object needing to be stored.
        :param save_option: Whether to allow creation, update, or both.
        :raises EntityAlreadyExists: If `save_option` is set to `SaveOption.create_only` and a
            matching entity already exists in the repository.
        :raises EntityNotFound: If `save_option` is set to `SaveOption.update_only` and a matching
            entity does not currently exist in the repository.
        """
        fail_reason: Optional[type[RepositoryError]] = None

        schema = Schemata.get(type(entity))
        if save_option == SaveOption.create_only:
            query = schema.insert_query
            fail_reason = EntityAlreadyExists
        elif save_option == SaveOption.create_or_update:
            query = schema.upsert_query
        else:  # save_option == SaveOption.update_only
            query = schema.update_query
            fail_reason = EntityNotFound

        try:
            if schema.table_name not in self._tables:
                self._con.execute(schema.table_definition)
                self._tables.add(schema.table_name)

            self._con.execute(query, self._safe_values(entity))
            if save_option == SaveOption.update_only:
                changes = self._con.execute("SELECT changes();").fetchone()["changes()"]
                if changes == 0:
                    raise EntitySaveFailed(reason=fail_reason() if fail_reason else None)
        except sqlite3.OperationalError as oe:
            print(oe)
            print(f"{entity=}")
            print(f"{save_option=}")
            print(f"{query=}")
            raise
        except sqlite3.IntegrityError as ie:
            raise EntitySaveFailed(reason=fail_reason() if fail_reason else None) from ie
        except sqlite3.ProgrammingError as pe:
            raise EntitySaveFailed(reason=RepositoryNotAccessible()) from pe

    @staticmethod
    def _safe_values(entity: Entity) -> dict[str, Any]:
        # schema = Schemata.get(type(entity))
        values = {}
        failed = {}
        for f in fields(type(entity)):
            original_val = getattr(entity, f.name)
            val = original_val
            if f.type is None:
                pass
            elif issubclass(type(f.type), type) and issubclass(f.type, Entity):
                val = val.entity_id
            elif callable(f.validator):
                val = f.validator(val)

            if isinstance(val, UUID):
                val = str(val)

            if not isinstance(val, (str, int, float)):
                try:
                    val = pickle.dumps(val)
                except pickle.PickleError as pe:
                    failed[f.name] = (original_val, val, pe)
                    warnings.warn(f"Cannot save field value for field '{f.name}', "
                                  f"value {original_val}")
                    continue

            values[f.name] = val

        if failed:
            raise EntitySaveFailed("Some values cannot be saved.", failed)
        return values

    def delete(self, entity_type: type[ET], entity_id: UUID, must_exist: bool = False) -> None:
        """
        Delete Entity with an ID matching the parameter.

        :param entity_type: The specific type of `Entity` expected.
        :param entity_id: The ID of the entity to be deleted.
        :param must_exist: If ``True``, raises an exception if no entity with the ID
            exists in the repository.  If ``False`` (default), proceed silently.
        :raises EntityNotFound: If `must_exist` is set to True and a matching
            entity does not already exist in the repository.
        """
        schema = Schemata.get(entity_type)
        try:
            cur = self._con.cursor()

            if must_exist:
                deleted = cur.execute(
                        schema.select_by_id_query,
                        {"entity_id": str(entity_id)}
                ).fetchall()
                if len(deleted) == 0:
                    raise EntityDeleteFailed(
                        reason=EntityNotFound(
                                f"Could not find {entity_type.__name__} with id {entity_id}."
                        )
                    )

            cur.execute(schema.delete_by_id_query, {"entity_id": str(entity_id)})

        except sqlite3.OperationalError as oe:
            if must_exist:
                raise EntityDeleteFailed(
                    reason=TypeError(f"No table for type {entity_type.__name__}.")
                ) from oe

        except sqlite3.ProgrammingError as pe:
            raise EntityDeleteFailed(
                    reason=RepositoryNotAccessible()
            ) from pe

    def find_entities(self, query: Query) -> list[Entity]:
        """
        Search for entities according to the query and return the matching list.

        .. attention::

            Not implemented

        :param query: Specification of the entities to return.
        :returns: A (possibly empty) list of the entities satisfying the query.
        :raises QueryError: If there are problems with the query.
        """
        raise NotImplementedError()

    def find_data(self, query: Query) -> list[Any]:
        """
        Search for data according to the query.

        .. attention::

            Not implemented

        The structure and format of the expected data is specified in the query.

        :param query: Specification of the data to return.
        :returns: A (possibly empty) list of the data entries satisfying the query.
        :raises QueryError: If there are problems with the query.
        """
        raise NotImplementedError()
